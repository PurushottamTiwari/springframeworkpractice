package com.example.springbootjdbc;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class VechileMapper implements RowMapper<com.example.springbootjdbc.Vechile> {

    @Override
    public Vechile mapRow(ResultSet resultSet, int i) throws SQLException {
            Vechile vechile = new Vechile();
            vechile.setVechile_No(resultSet.getString("VECHILE_NO"));
            vechile.setColor(resultSet.getString("COLOR"));
            vechile.setWheel(resultSet.getInt("WHEEL"));
            vechile.setSeat(resultSet.getInt("SEAT"));
        return vechile;
    }
}
