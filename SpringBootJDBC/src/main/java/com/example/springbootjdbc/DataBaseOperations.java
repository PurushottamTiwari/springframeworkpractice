package com.example.springbootjdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.*;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class DataBaseOperations {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    VechileMapper vechileMapper;



    private static final String INSERT_SQL = "insert into VECHILE values (?,?,?,?)";

    private static final String GET_SQL = "SELECT * FROM VECHILE";

    public void insertFirstRecord(Vechile vechile){
        System.out.println("****** Insert Start *********");
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
               PreparedStatement prepareStatement = connection.prepareStatement(INSERT_SQL);
               prepareStatement.setString(1,vechile.getVechile_No());
               prepareStatement.setString(2, vechile.getColor());
               prepareStatement.setInt(3, vechile.getWheel());
               prepareStatement.setInt(4, vechile.getSeat());
               return prepareStatement;
            }
        });
        System.out.println("****** Insert End *********");
    }

    public void insertSecondRecord(Vechile vechile){
        System.out.println("****** Second Insert Start *********");
        jdbcTemplate.update(INSERT_SQL, new PreparedStatementSetter(){

            @Override
            public void setValues(PreparedStatement preparedStatement) throws SQLException {
                preparedStatement.setString(1,vechile.getVechile_No());
                preparedStatement.setString(2, vechile.getColor());
                preparedStatement.setInt(3, vechile.getWheel());
                preparedStatement.setInt(4, vechile.getSeat());
            }
        });
        System.out.println("****** Second Insert End *********");
    }

    public void insertThirdRecord(Vechile vechile){
        System.out.println("****** Third Insert Start *********");
        jdbcTemplate.update(INSERT_SQL, vechile.getVechile_No(), vechile.getColor(), vechile.getWheel(), vechile.getSeat());
        System.out.println("****** Third Insert End *********");
    }

    public  void  showAllRecordsFirstTime(){
        System.out.println("****** First Fetch *********");
        jdbcTemplate.query(GET_SQL, new RowCallbackHandler() {
            @Override
            public void processRow(ResultSet resultSet) throws SQLException {
                Vechile vechile = new Vechile();
                vechile.setVechile_No(resultSet.getString("VECHILE_NO"));
                vechile.setColor(resultSet.getString("COLOR"));
                vechile.setWheel(resultSet.getInt("WHEEL"));
                vechile.setSeat(resultSet.getInt("SEAT"));
                System.out.println(vechile);
            }
        });
        System.out.println("****** First Fetch *********");
    }

    public void  showAllRecordsSecondTime(){
        System.out.println("****** Second Fetch *********");
        List<Vechile> vechileList = jdbcTemplate.query(GET_SQL, vechileMapper);
        showVechiles(vechileList);
        System.out.println("****** Second Fetch *********");
    }

    public void showAllRecordsThirdTime(){
        System.out.println("****** Third Fetch *********");
        List<Vechile> vechileList = jdbcTemplate.query(GET_SQL, BeanPropertyRowMapper.newInstance(Vechile.class));
        showVechiles(vechileList);
        System.out.println("****** Third Fetch *********");
    }

    public void showVechiles(List<Vechile> vechileList){
        vechileList.stream().forEach(System.out::println);
    }

}
