package com.example.springbootjdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class SpringBootJdbcApplication {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(SpringBootJdbcApplication.class, args);
        DataBaseOperations dataBaseOperations = applicationContext.getBean(DataBaseOperations.class);
        Vechile vechile = new Vechile("1233","Blue",1,1);
        dataBaseOperations.insertFirstRecord(vechile);
        vechile = new Vechile("4566","Blue",2,2);
        dataBaseOperations.insertSecondRecord(vechile);
        vechile = new Vechile("7896","Blue",3,3);
        dataBaseOperations.insertThirdRecord(vechile);

        dataBaseOperations.showAllRecordsFirstTime();
        dataBaseOperations.showAllRecordsSecondTime();
        dataBaseOperations.showAllRecordsThirdTime();
    }

}
