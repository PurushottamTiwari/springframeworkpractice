package org.hibernate.test;

import net.bytebuddy.asm.Advice;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.Oracle12cDialect;
import org.practice.dataacess.DatabaseConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import java.util.Properties;


@Configuration
public class HibernateConfiguration  extends DatabaseConfiguration {


    @Bean
    public HibernateOperations  getHibernateOperations(SessionFactory sessionFcatory){
        HibernateOperations hibernateOperations = new HibernateOperations(sessionFcatory);
        return hibernateOperations;
    }




    @Bean
    public LocalSessionFactoryBean getLocalSessionFactoryBean(){
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(getOracleDataSource());
        localSessionFactoryBean.setAnnotatedClasses(VechileEntity.class);
        localSessionFactoryBean.setHibernateProperties(getHibernateProperties());
        return localSessionFactoryBean;
    }


    public Properties  getHibernateProperties(){
        Properties properties = new Properties();
        properties.setProperty(AvailableSettings.DIALECT,   Oracle10gDialect.class.getName());
        properties.setProperty(AvailableSettings.HBM2DDL_AUTO,"update");
        properties.setProperty(AvailableSettings.SHOW_SQL,"true");
        properties.setProperty(AvailableSettings.FORMAT_SQL,"true");
        return properties;
    }


}
