package org.hibernate.test;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Table(name = "VECHILE")
@SequenceGenerator(sequenceName = "VECHILE_SEQ", name = "VECHILE_SEQ", allocationSize = 1, initialValue = 1)
@DynamicInsert
@DynamicUpdate
public class VechileEntity {

    @Id
    @Column(name = "VECHILE_NO")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VECHILE_SEQ")
    private int vechile_No;

    @Column(name = "COLOR")
    private String color;

    @Column(name = "WHEEL")
    private int wheel;

    @Column(name = "SEAT")
    private int seat;

    public VechileEntity() {
    }

    public VechileEntity(String color, int wheel, int seat) {
        this.color = color;
        this.wheel = wheel;
        this.seat = seat;
    }

    public int getVechile_No() {
        return vechile_No;
    }

    public void setVechile_No(int vechile_No) {
        this.vechile_No = vechile_No;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWheel() {
        return wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    @Override
    public String toString() {
        return "VechileEntity{" +
                "vechile_No=" + vechile_No +
                ", color='" + color + '\'' +
                ", wheel=" + wheel +
                ", seat=" + seat +
                '}';
    }
}
