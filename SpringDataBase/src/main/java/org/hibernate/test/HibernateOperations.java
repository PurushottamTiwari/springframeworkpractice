package org.hibernate.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.springframework.stereotype.Component;

import java.util.List;


public class HibernateOperations {


    SessionFactory sessionFactory;
    Transaction transaction;
    Session session;

    public HibernateOperations(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void saveOrUpdateVechile(VechileEntity vechileEntity){
        session =  sessionFactory.openSession();
        transaction = session.getTransaction();
        try{
            transaction.begin();
            session.saveOrUpdate(vechileEntity);
            transaction.commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public  void deleteVechile(int vechile_No){
        session = sessionFactory.openSession();
        transaction = session.getTransaction();
        try {
            transaction.begin();
            VechileEntity vechileEntity  = session.get(VechileEntity.class,vechile_No);
            session.delete(vechileEntity);
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public  VechileEntity getVechile(int vechile_No){
        session = sessionFactory.openSession();
        VechileEntity vechileEntity = null;
        try {
             vechileEntity = session.get(VechileEntity.class,vechile_No);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return  vechileEntity;
    }

    public List<VechileEntity> getAllVechiles(){
        session = sessionFactory.openSession();
        List<VechileEntity> vechileEntities = null;
        try{
            vechileEntities = session.createQuery("SELECT v FROM VechileEntity v", VechileEntity.class).list();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return vechileEntities;
    }
}
