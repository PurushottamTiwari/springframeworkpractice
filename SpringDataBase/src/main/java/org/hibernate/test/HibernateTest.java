package org.hibernate.test;

import org.practice.dataacess.DataBaseOperations;
import org.practice.dataacess.DataBaseTest;
import org.practice.dataacess.DatabaseConfiguration;
import org.practice.dataacess.Vechile;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class HibernateTest  {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =  new AnnotationConfigApplicationContext(HibernateConfiguration.class);

        HibernateOperations hibernateOperations = applicationContext.getBean(HibernateOperations.class);

        VechileEntity vechile1 = new VechileEntity("Blue",1,1);
        VechileEntity vechile2 = new VechileEntity("Green",2,2);
        VechileEntity vechile3 = new VechileEntity("Red",3,3);

        hibernateOperations.saveOrUpdateVechile(vechile1);
        hibernateOperations.saveOrUpdateVechile(vechile2);
        hibernateOperations.saveOrUpdateVechile(vechile3);

        List<VechileEntity> vechileEntityList = hibernateOperations.getAllVechiles();
       System.out.println("SIZE :"+vechileEntityList.size());


        VechileEntity vechileEntity = hibernateOperations.getVechile(vechileEntityList.get(2).getVechile_No());
        vechileEntity.setColor("Voilet");
        hibernateOperations.saveOrUpdateVechile(vechileEntity);


        hibernateOperations.deleteVechile(vechileEntityList.get(1).getVechile_No());

        vechileEntityList = hibernateOperations.getAllVechiles();
        vechileEntityList.stream().forEach(System.out::println);

    }
}
