package org.jpa;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.test.VechileEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import java.util.List;


public class JPAOperations {

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;
    Session session;

    public JPAOperations(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public void saveOrUpdateVechile(VechileEntity vechileEntity){
        entityManager = entityManagerFactory.createEntityManager();
        try{
           EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            entityManager.merge(vechileEntity);
            transaction.commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
    }

    public  void deleteVechile(int vechile_No){
        entityManager = entityManagerFactory.createEntityManager();
        try {
            EntityTransaction transaction = entityManager.getTransaction();
            transaction.begin();
            VechileEntity vechileEntity  = entityManager.find(VechileEntity.class, vechile_No);
            entityManager.remove(vechileEntity);
            transaction.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
    }

    public  VechileEntity getVechile(int vechile_No){
        entityManager = entityManagerFactory.createEntityManager();
        VechileEntity vechileEntity = null;
        try {

             vechileEntity  = entityManager.find(VechileEntity.class, vechile_No);


        }catch (Exception e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
        return vechileEntity;
    }

    public List<VechileEntity> getAllVechiles(){
        List<VechileEntity> vechileEntities = null;
        entityManager = entityManagerFactory.createEntityManager();

        try {

            vechileEntities  = entityManager.createQuery("select vechile from VechileEntity vechile", VechileEntity.class).getResultList();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            entityManager.close();
        }
        return vechileEntities;
    }
}
