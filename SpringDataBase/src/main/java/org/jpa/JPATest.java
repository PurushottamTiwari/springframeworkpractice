package org.jpa;

import org.hibernate.test.HibernateConfiguration;
import org.hibernate.test.HibernateOperations;
import org.hibernate.test.VechileEntity;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class JPATest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =  new AnnotationConfigApplicationContext(JPAConfiguration.class);

        JPAOperations jpaOperations = applicationContext.getBean(JPAOperations.class);

        VechileEntity vechile1 = new VechileEntity("Blue",1,1);
        VechileEntity vechile2 = new VechileEntity("Green",2,2);
        VechileEntity vechile3 = new VechileEntity("Red",3,3);

        jpaOperations.saveOrUpdateVechile(vechile1);
        jpaOperations.saveOrUpdateVechile(vechile2);
        jpaOperations.saveOrUpdateVechile(vechile3);

        List<VechileEntity> vechileEntityList = jpaOperations.getAllVechiles();
       System.out.println("SIZE :"+vechileEntityList.size());


        VechileEntity vechileEntity = jpaOperations.getVechile(vechileEntityList.get(2).getVechile_No());
        vechileEntity.setColor("Voilet");
        jpaOperations.saveOrUpdateVechile(vechileEntity);


        jpaOperations.deleteVechile(vechileEntityList.get(1).getVechile_No());

        vechileEntityList = jpaOperations.getAllVechiles();
        vechileEntityList.stream().forEach(System.out::println);

    }
}
