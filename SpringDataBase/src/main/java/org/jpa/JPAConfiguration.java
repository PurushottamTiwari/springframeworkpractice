package org.jpa;

import org.hibernate.dialect.Oracle10gDialect;
import org.practice.dataacess.DatabaseConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;

@Configuration
public class JPAConfiguration extends DatabaseConfiguration {

    @Bean
    public JPAOperations getJPAJpaOperations(EntityManagerFactory entityManagerFactory){
        JPAOperations jpaOperations = new JPAOperations(entityManagerFactory);
        return jpaOperations;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean getLocalContainerEntityManagerFcatoryBean(){
       LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
       entityManagerFactoryBean.setDataSource(getOracleDataSource());
       entityManagerFactoryBean.setPersistenceUnitName("VECHILE");
       entityManagerFactoryBean.setJpaVendorAdapter(getJpaVendorAdapter());
       return entityManagerFactoryBean;
    }

    @Bean
    public JpaVendorAdapter  getJpaVendorAdapter(){
        HibernateJpaVendorAdapter  hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(true);
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabasePlatform(Oracle10gDialect.class.getName());
        return hibernateJpaVendorAdapter;
    }
}
