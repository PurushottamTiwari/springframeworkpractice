package org.practice.dataacess;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class DataBaseTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext =  new AnnotationConfigApplicationContext(DatabaseConfiguration.class);

        DataBaseOperations dataBaseOperations = applicationContext.getBean(DataBaseOperations.class);

        Vechile vechile = new Vechile("123","Blue",1,1);
        dataBaseOperations.insertFirstRecord(vechile);
        vechile = new Vechile("456","Blue",2,2);
        dataBaseOperations.insertSecondRecord(vechile);
        vechile = new Vechile("789","Blue",3,3);
        dataBaseOperations.insertThirdRecord(vechile);

        dataBaseOperations.showAllRecordsFirstTime();
        dataBaseOperations.showAllRecordsSecondTime();
        dataBaseOperations.showAllRecordsThirdTime();

        applicationContext.registerShutdownHook();

    }
}
