package org.practice.dataacess;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
@ComponentScan("org.practice.dataacess")
public class DatabaseConfiguration {

    @Value("${spring.datasource.driver-class}")
    private String oracle_DriverClassname;

    @Value("${spring.datasource.url}")
    private String oracle_DatabaseUrl;

    @Value("${spring.datasource.username}")
    private String oracle_DatabaseUserName;

    @Value("${spring.datasource.password}")
    private String oracle_DatabasePassword;


    @Bean
    public DataSource  getOracleDataSource(){
        DriverManagerDataSource driverManagerDataSource =  new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(oracle_DriverClassname);
        driverManagerDataSource.setPassword(oracle_DatabasePassword);
        driverManagerDataSource.setUsername(oracle_DatabaseUserName);
        driverManagerDataSource.setUrl(oracle_DatabaseUrl);
        return driverManagerDataSource;
    }

    @Bean
    @DependsOn("getOracleDataSource")
    public JdbcTemplate  getOracleJDBCTemplate(){
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(getOracleDataSource());
        return jdbcTemplate;
    }

}
