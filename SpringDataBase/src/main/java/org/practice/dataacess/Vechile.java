package org.practice.dataacess;

public class Vechile {

    private String  vechile_No;
    private String  color;
    private int     wheel;
    private int     seat;


    public Vechile() {
    }

    public Vechile(String vechile_No, String color, int wheel, int seat) {
        this.vechile_No = vechile_No;
        this.color = color;
        this.wheel = wheel;
        this.seat = seat;
    }

    public String getVechile_No() {
        return vechile_No;
    }

    public void setVechile_No(String vechile_No) {
        this.vechile_No = vechile_No;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getWheel() {
        return wheel;
    }

    public void setWheel(int wheel) {
        this.wheel = wheel;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    @Override
    public String toString() {
        return "Vechile{" +
                "vechile_No='" + vechile_No + '\'' +
                ", color='" + color + '\'' +
                ", wheel=" + wheel +
                ", seat=" + seat +
                '}';
    }
}
