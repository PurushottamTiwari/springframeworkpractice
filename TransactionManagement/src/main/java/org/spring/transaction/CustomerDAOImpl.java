package org.spring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

@Component

@DependsOn("getTransactionManager")
public class CustomerDAOImpl implements CustomerDAO {

    private static final String CUSTOMER_INSERT_SQL = "INSERT INTO CUSTOMER values (?, ?)";
    private static final String ADDRESS_INSERT_SQL = "INSERT INTO ADDRESS values (?, ?, ?)";
    DataSourceTransactionManager dataSourceTransactionManager;
    JdbcTemplate jdbcTemplate;

    public void create(Customer customer) {
        DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
        transactionDefinition.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionDefinition.setTimeout(3);
        TransactionStatus transactionStatus = dataSourceTransactionManager.getTransaction(transactionDefinition);
        try {
            jdbcTemplate.update(CUSTOMER_INSERT_SQL, customer.getId(), customer.getName());
            jdbcTemplate.update(ADDRESS_INSERT_SQL, customer.getAddress().getId(), customer.getAddress().getAddress(), customer.getAddress().getCountry());
            dataSourceTransactionManager.commit(transactionStatus);
            System.out.println("Transaction Completed Successfully");
        } catch (Exception e) {
            System.out.println("Transaction Failed" + e.getMessage());
            e.printStackTrace();
            dataSourceTransactionManager.rollback(transactionStatus);

        }
    }

    public DataSourceTransactionManager getDataSourceTransactionManager() {
        return dataSourceTransactionManager;
    }

    @Autowired
    public void setDataSourceTransactionManager(DataSourceTransactionManager dataSourceTransactionManager) {
        this.dataSourceTransactionManager = dataSourceTransactionManager;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
