package org.spring.transaction;

public interface CustomerManager {

    void createCustomer(Customer cust);
}