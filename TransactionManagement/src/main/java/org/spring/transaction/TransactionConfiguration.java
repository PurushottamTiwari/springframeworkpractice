package org.spring.transaction;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

@Configuration
@PropertySource("classpath:database.properties")
@ComponentScan("org.spring.transaction")
@EnableTransactionManagement
public class TransactionConfiguration {

    @Value("${spring.datasource.driver-class}")
    private String oracle_DriverClassname;

    @Value("${spring.datasource.url}")
    private String oracle_DatabaseUrl;

    @Value("${spring.datasource.username}")
    private String oracle_DatabaseUserName;

    @Value("${spring.datasource.password}")
    private String oracle_DatabasePassword;


    @Bean
    public DataSource getOracleDataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName(oracle_DriverClassname);
        driverManagerDataSource.setPassword(oracle_DatabasePassword);
        driverManagerDataSource.setUsername(oracle_DatabaseUserName);
        driverManagerDataSource.setUrl(oracle_DatabaseUrl);
        return driverManagerDataSource;
    }

    @Bean
    @DependsOn("getOracleDataSource")
    public JdbcTemplate getOracleJDBCTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(getOracleDataSource());
        return jdbcTemplate;
    }

    @Bean
    @DependsOn("getOracleDataSource")
    public PlatformTransactionManager getTransactionManager() {
        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
        dataSourceTransactionManager.setDataSource(getOracleDataSource());
        return dataSourceTransactionManager;
    }

    @Bean
    public TransactionTemplate getTransactionTemplate() {
        TransactionTemplate transactionTemplate = new TransactionTemplate();
        transactionTemplate.setTransactionManager(getTransactionManager());
        transactionTemplate.setTimeout(5);
        transactionTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_READ_COMMITTED);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        return transactionTemplate;
    }

    @Bean
    public CustomerDAO getCustomerDAOImplWithDeclarativeTransaction(){
        CustomerDAOImplWithDeclarativeTransaction customerDAOImplWithDeclarativeTransaction =  new CustomerDAOImplWithDeclarativeTransaction(getOracleJDBCTemplate());
        return customerDAOImplWithDeclarativeTransaction;
    }

}
