package org.spring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public class CustomerDAOImplWithDeclarativeTransaction implements CustomerDAO {

    private static final String CUSTOMER_INSERT_SQL = "INSERT INTO CUSTOMER values (?, ?)";
    private static final String ADDRESS_INSERT_SQL = "INSERT INTO ADDRESS values (?, ?, ?)";


    JdbcTemplate jdbcTemplate;

    public CustomerDAOImplWithDeclarativeTransaction(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
    public void create(Customer customer) {
        try {
            jdbcTemplate.update(CUSTOMER_INSERT_SQL, customer.getId(), customer.getName());
            jdbcTemplate.update(ADDRESS_INSERT_SQL, customer.getAddress().getId(), customer.getAddress().getAddress(), customer.getAddress().getCountry());
            System.out.println("Transaction Completed Successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
