package org.spring.transaction;

public interface CustomerDAO {

    void create(Customer customer);
}
