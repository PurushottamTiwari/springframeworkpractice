package org.spring.transaction;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TransactionManagerMainForDeclarativeTransaction {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TransactionConfiguration.class);

        CustomerDAO customerDAO = (CustomerDAO) ctx.getBean("getCustomerDAOImplWithDeclarativeTransaction");

        Customer cust = createDummyCustomer();
        customerDAO.create(cust);

        ctx.close();
    }

    private static Customer createDummyCustomer() {
        Customer customer = new Customer();
        customer.setId(5);
        customer.setName("Pankaj");
        Address address = new Address();
        address.setId(5);
        address.setCountry("India");
        // setting value more than 20 chars, so that SQLException occurs
        address.setAddress("Albany Dr");
        customer.setAddress(address);
        return customer;
    }
}
