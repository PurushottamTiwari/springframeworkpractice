package org.spring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

@Component

@DependsOn("getTransactionManager")
public class CustomerDAOImplWithTransactionTemplate implements CustomerDAO {


    private static final String CUSTOMER_INSERT_SQL = "INSERT INTO CUSTOMER values (?, ?)";
    private static final String ADDRESS_INSERT_SQL = "INSERT INTO ADDRESS values (?, ?, ?)";
    TransactionTemplate transactionTemplate;
    JdbcTemplate jdbcTemplate;

    public TransactionTemplate getTransactionTemplate() {
        return transactionTemplate;
    }

    @Autowired
    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Autowired
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(Customer customer) {
        try {
            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
                    jdbcTemplate.update(CUSTOMER_INSERT_SQL, customer.getId(), customer.getName());
                    jdbcTemplate.update(ADDRESS_INSERT_SQL, customer.getAddress().getId(), customer.getAddress().getAddress(), customer.getAddress().getCountry());
                }
            });
            System.out.println("Transaction Completed Successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
