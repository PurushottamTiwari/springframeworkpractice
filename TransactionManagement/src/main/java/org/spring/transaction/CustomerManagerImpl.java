package org.spring.transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

@Component
@DependsOn("customerDAOImpl")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    CustomerDAOImplWithTransactionTemplate customerDAO;

    public void createCustomer(Customer cust) {
        customerDAO.create(cust);
    }
}
