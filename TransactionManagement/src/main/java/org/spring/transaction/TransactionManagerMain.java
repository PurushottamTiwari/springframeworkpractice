package org.spring.transaction;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TransactionManagerMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(TransactionConfiguration.class);

        CustomerManager customerManager = ctx.getBean(CustomerManagerImpl.class);

        Customer cust = createDummyCustomer();
        customerManager.createCustomer(cust);

        ctx.close();
    }

    private static Customer createDummyCustomer() {
        Customer customer = new Customer();
        customer.setId(6);
        customer.setName("Pankaj");
        Address address = new Address();
        address.setId(6);
        address.setCountry("India");
        // setting value more than 20 chars, so that SQLException occurs
        address.setAddress("Albany Dr, San Jose, CA 95129");
        customer.setAddress(address);
        return customer;
    }
}
