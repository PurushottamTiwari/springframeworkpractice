package com.example.springdatajpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RepositoryOperations {

    @Autowired
    VechileRepository vechileRepository;

    public void saveOrUpdateVechile(VechileEntity vechileEntity) {
        vechileRepository.save(vechileEntity);
    }

    public void deleteVechile(int vechile_No) {
        Optional<VechileEntity> vechileEntity = vechileRepository.findById(vechile_No);
        if (vechileEntity.isPresent()) vechileRepository.delete(vechileEntity.get());
    }

    public VechileEntity getVechile(int vechile_No) {
        Optional<VechileEntity> vechileEntity = vechileRepository.findById(vechile_No);
        return vechileEntity.get();
    }

    public List<VechileEntity> getAllVechiles() {
        List<VechileEntity> vechileEntityList = vechileRepository.findAllByVechile_No();
        return vechileEntityList;
    }

}
