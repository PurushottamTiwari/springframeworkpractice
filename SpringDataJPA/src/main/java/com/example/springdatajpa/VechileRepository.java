package com.example.springdatajpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VechileRepository extends JpaRepository<VechileEntity, Integer> {
  @Query("FROM VechileEntity  ORDER BY vechile_No asc")
    public List<VechileEntity> findAllByVechile_No();
}

