package com.example.springdatajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.List;

@SpringBootApplication
@EnableJpaRepositories
public class SpringDataJpaApplication {

    public static void main(String[] args) {

        ApplicationContext applicationContext = SpringApplication.run(SpringDataJpaApplication.class, args);


        RepositoryOperations hibernateOperations = applicationContext.getBean(RepositoryOperations.class);

        VechileEntity vechile1 = new VechileEntity("Blue",1,1);
        VechileEntity vechile2 = new VechileEntity("Green",2,2);
        VechileEntity vechile3 = new VechileEntity("Red",3,3);

        hibernateOperations.saveOrUpdateVechile(vechile1);
        hibernateOperations.saveOrUpdateVechile(vechile2);
        hibernateOperations.saveOrUpdateVechile(vechile3);

        List<VechileEntity> vechileEntityList = hibernateOperations.getAllVechiles();
        System.out.println("SIZE :"+vechileEntityList.size());


        VechileEntity vechileEntity = hibernateOperations.getVechile(vechileEntityList.get(2).getVechile_No());
        vechileEntity.setColor("Voilet");
        hibernateOperations.saveOrUpdateVechile(vechileEntity);


        hibernateOperations.deleteVechile(vechileEntityList.get(1).getVechile_No());

        vechileEntityList = hibernateOperations.getAllVechiles();
        vechileEntityList.stream().forEach(System.out::println);

    }

}
