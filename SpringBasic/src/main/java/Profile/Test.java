package Profile;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Profile;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext();
        annotationConfigApplicationContext.getEnvironment().setActiveProfiles("two");
        annotationConfigApplicationContext.scan("Profile");
        annotationConfigApplicationContext.refresh();
        BeanTwo  beanTwo = annotationConfigApplicationContext.getBean(BeanTwo.class);
        System.out.println(beanTwo);
    }
}
