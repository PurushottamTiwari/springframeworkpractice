package Profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("one")
public class BeanOne {

    @Override
    public String toString() {
        return "BeanOne{}";
    }
}
