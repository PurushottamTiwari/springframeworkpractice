package Profile;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("two")
public class BeanTwo {
    @Override
    public String toString() {
        return "BeanTwo{}";
    }
}
