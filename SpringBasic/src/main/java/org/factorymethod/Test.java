package org.factorymethod;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(org.factorymethod.Config.class);
        VechileFactory vechileFactory = annotationConfigApplicationContext.getBean(VechileFactory.class);
        Vechile vechile = null;
        try {
            vechile = vechileFactory.getObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(vechile.createVechile());
    }
}

