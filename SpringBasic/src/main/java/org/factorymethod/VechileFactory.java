package org.factorymethod;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

@Component
public class VechileFactory implements FactoryBean<Vechile> {

    @Override
    public Vechile getObject() throws Exception {

                return new Bus();
    }

    @Override
    public Class<?> getObjectType() {
           return Vechile.class;
    }

    @Override
    public boolean isSingleton() {
        return  false;
    }
}
