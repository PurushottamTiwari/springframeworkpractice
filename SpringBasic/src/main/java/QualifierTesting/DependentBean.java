package QualifierTesting;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
public class DependentBean {

    @Autowired
    @Qualifier("testbean2")
    private BeanInterface beanInterface;

    @Override
    public String toString() {
        return "DependentBean{" +
                "beanInterface=" + beanInterface +
                '}';
    }
}
