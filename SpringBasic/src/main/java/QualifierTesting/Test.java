package QualifierTesting;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext annotationConfigApplicationContext =  new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
        DependentBean dependentBean = annotationConfigApplicationContext.getBean(DependentBean.class);
        System.out.println(dependentBean);
    }
}
