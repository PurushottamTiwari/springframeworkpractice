package QualifierTesting;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class ApplicationConfiguration {

    @Qualifier("testbean")
    @Bean
    public BeanInterface getTestBean(){
        return new TestBean();
    }

    @Qualifier("testbean2")
    @Bean
    public BeanInterface getTestBean2(){
         return new TestBean2();
    }
}
